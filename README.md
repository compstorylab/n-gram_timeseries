# ngram_query

Tools to query ngram timeseries from twitter language databases.

To get started make sure you have an anaconda distribution of Python 3, with pymongo installed,

`conda install pymongo`

Also make sure to get the password.
